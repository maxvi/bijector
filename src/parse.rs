//! Parsing.

use Number;

/// The grammar from `grammar.rustpeg`.
#[allow(unconditional_recursion)]
mod grammar {
    include!(concat!(env!("OUT_DIR"), "/grammar.rs"));
}

/// Variable names.
pub type Store = String;
/// Sequence of statements.
pub type Proc = Vec<Stat>;
/// Procedures and procedure bodies.
pub type Program = Vec<(Func, Proc)>;

/// The expression AST.
#[derive(Debug)]
pub enum Expr {
    Number(Number),
    Load(Store),
    Neg(Box<Expr>),
    Not(Box<Expr>),
    Le(Box<Expr>, Box<Expr>),
    Leq(Box<Expr>, Box<Expr>),
    Gr(Box<Expr>, Box<Expr>),
    Geq(Box<Expr>, Box<Expr>),
    Eq(Box<Expr>, Box<Expr>),
    Neq(Box<Expr>, Box<Expr>),
    Add(Box<Expr>, Box<Expr>),
    Sub(Box<Expr>, Box<Expr>),
    Mul(Box<Expr>, Box<Expr>),
    Div(Box<Expr>, Box<Expr>),
    Rem(Box<Expr>, Box<Expr>),
    And(Box<Expr>, Box<Expr>),
    Or(Box<Expr>, Box<Expr>),
    Xor(Box<Expr>, Box<Expr>),
}

/// Statements.
#[derive(Debug)]
pub enum Stat {
    /// Add a number to a store.
    Add(Store, Expr),
    /// Subtract a number from a store.
    Sub(Store, Expr),
    /// Call a procedure.
    Call(Func),
    /// Inverse-call a procedure.
    Uncall(Func),
    /// Run `body` if `in_condition` is non-zero. Otherwise, run `body_else`.
    ///
    /// Afterwards, `out_condition` shall be non-zero if `body` ran and zero if `body_else` ran.
    If {
        in_condition: Expr,
        out_condition: Expr,
        body: Proc,
        body_else: Proc,
    },
    /// Run `body` until `cond` is non-zero.
    ///
    /// If `cond` is not non-zero at start, throw an error.
    Until { cond: Expr, body: Proc },
    /// Read from stdin to a store, which is zero.
    Read(Store),
    /// Print and zero a store.
    Print(Store),
}

/// A "function", i.e. a procedure name and parameters.
///
/// This structure is used to both represent function calls and function declarations.
#[derive(Debug)]
pub struct Func {
    /// The procedure name.
    pub name: String,
    /// Parameter names.
    pub params: Vec<Store>,
}

/// Parse `s` into a program.
pub fn parse(s: &str) -> Program {
    grammar::prog(s).unwrap_or_else(|err| {
        error!(
            "Parsing error at {}:{}: expected one of {:?}",
            err.line, err.column, err.expected
        )
    })
}
