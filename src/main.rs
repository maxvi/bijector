//! Bijector: The reversible programming language.

extern crate clap;
extern crate num;
extern crate take_mut;
use clap::{App, Arg};
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

/// Print error (specified in formatting style same as `println!`) and exit program.
macro_rules! error {
    ($($arg:tt)*) => {{
        eprint!("ERROR: ");
        eprintln!($($arg)*);
        ::std::process::exit(1);
    }};
}

mod parse;
mod state;

/// The integer type used throughout.
type Number = num::BigInt;

fn main() {
    // The CLI specified through Clap-rs.
    let matches = App::new("Bijector")
        .version("1.0")
        .author("Max Vistrup")
        .about("Interpreter for the reversible programming language, Bijector.")
        .arg(
            Arg::with_name("reverse")
                .short("r")
                .long("reverse")
                .help("Runs program in reverse")
                .takes_value(false),
        )
        .arg(
            Arg::with_name("INPUT")
                .help("The program file")
                .required(true)
                .index(1),
        )
        .get_matches();

    // Read the arguments.
    let path = Path::new(matches.value_of("INPUT").unwrap());
    let reverse = if matches.is_present("reverse") {
        true
    } else {
        false
    };

    // Load the input file.
    let mut input = String::new();
    File::open(path)
        .unwrap_or_else(|err| error!("IO error: {}.", err))
        .read_to_string(&mut input)
        .unwrap_or_else(|err| error!("IO error: {}.", err));

    // Execute the program.
    state::exec(&parse::parse(&input), reverse);
}
