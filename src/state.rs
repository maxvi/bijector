//! Program state and execution.

use num::{Signed, Zero};
use parse::{Expr, Func, Proc, Program, Stat};
use std::collections::HashMap;
use std::{fmt, io};
use take_mut;
use Number;

/// Execute `prog`'s `main` procedure.
///
/// Executes its inverse if `reverse` is `true`.
pub fn exec(prog: &Program, reverse: bool) {
    let mut state = State::new(prog);
    let main = state
        .procs
        .get("main")
        .unwrap_or_else(|| error!("Program lacks 'main' procedure."))
        .0;
    state.call(main, reverse);
}

/// Convert a boolean into a number (1 for `true`, 0 otherwise).
fn into_num(b: bool) -> Number {
    if b {
        1.into()
    } else {
        0.into()
    }
}

/// Convert a number into a boolean (`true` if non-zero, `false` otherwise).
fn into_bool(n: Number) -> bool {
    !n.is_zero()
}

/// A call stack frame.
///
/// This roughly represents a function call with the scope of variables it has and the name of the
/// function.
struct Frame<'a> {
    /// The name of the function it represents.
    name: &'a str,
    /// The variables it stores.
    vars: HashMap<&'a str, Binding<'a>>,
}

/// The program state.
#[derive(Default)]
struct State<'a> {
    /// The procedures.
    ///
    /// This stores the procedures with keys being the procedure names and values being `(func,
    /// proc)` tuples, where `func` represents the declared arguments and `proc` represents the
    /// procedure body.
    procs: HashMap<&'a str, (&'a Func, &'a Proc)>,
    /// The call stack.
    ///
    /// Upon each new call, a new frame is added. Such frame contains the values and parameters of
    /// a function scope. Only values from the highest frame should be read directly (i.e.
    /// variable privacy).
    stack: Vec<Frame<'a>>,
}

/// A variable binding.
enum Binding<'a> {
    /// A value store.
    Val(Number),
    /// A reference to a value from another stack frame.
    Ref(Reference<'a>),
}

/// A reference to a value.
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
struct Reference<'a> {
    /// The frame the referenced value resides on.
    frame: usize,
    /// The name of the references value.
    name: &'a str,
}

impl<'a> fmt::Display for Reference<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} (frame {})", self.name, self.frame)
    }
}

impl<'a> State<'a> {
    /// Create a new program state with procedures loaded from `prog`.
    fn new(prog: &'a Program) -> State<'a> {
        // Empty state.
        let mut state = State::default();

        // Load procedures.
        for (func, pr) in prog {
            if state.procs.insert(&func.name, (&func, &pr)).is_some() {
                error!("Duplicate definition of procedure '{}'.", func.name);
            }
        }

        state
    }

    /// Get name of the procedure currently being executed.
    pub fn current_proc(&self) -> &str {
        self.stack.last().unwrap().name
    }

    /// Pop the highest stack frame.
    ///
    /// This ensures that all the values (i.e. not references) on the frame are zeroed. Otherwise,
    /// an error is thrown.
    fn pop_stack(&mut self) {
        let frame = self.stack.pop().unwrap();

        // Ensure all the values are zeroed.
        for (name, val) in frame.vars.iter() {
            match val {
                &Binding::Val(ref n) if !n.is_zero() => error!(
                    "'{}' is not zero ({}) when going out of scope. (in '{}')",
                    name, n, frame.name
                ),
                _ => (),
            }
        }
    }

    /// Get reference to variable from current scope named `name`.
    fn get_ref(&mut self, name: &'a str) -> Reference<'a> {
        // Find the variable in the first frame for function scoping.
        match self.stack
            .last()
            .unwrap_or_else(|| error!("Cannot read '{}' before program initializiation.", name))
            .vars
            .get(name)
        {
            // The variable was found as a value in the current scope. Return a reference to the
            // variable.
            Some(Binding::Val(..)) => {
                return Reference {
                    frame: self.stack.len() - 1,
                    name,
                }
            }
            // Follow the reference; return the reference to what it references.
            Some(Binding::Ref(ref reference)) => return reference.clone(),
            // See below.
            None => (),
        }

        // None was found, so we create a variable on the top frame with value 0. (Not included in
        // above match to avoid aliasing mutable variables; necessary for borrowck.)
        self.stack
            .last_mut()
            .unwrap()
            .vars
            .insert(name, Binding::Val(0.into()));
        Reference {
            // Highest frame.
            frame: self.stack.len() - 1,
            name,
        }
    }

    /// Load value behind `reference`.
    fn load(&self, reference: Reference) -> Number {
        if let Binding::Val(n) = self.stack[reference.frame]
            .vars
            .get(reference.name)
            .unwrap()
        {
            n.clone()
        } else {
            // Not possible.
            panic!("Reference to reference.");
        }
    }

    /// Print and zero `reference` to stdout (if `reverse` is `false`) or read from stdin to
    /// `reference` (if `reverse` is `true`).
    fn print(&mut self, reference: Reference, reverse: bool) {
        if reverse {
            // Cursor for stderr.
            eprint!("> ");
            // Read line from stdin.
            let mut str = String::new();
            let current_proc = self.current_proc().to_string();
            io::stdin().read_line(&mut str).unwrap_or_else(|err| {
                error!(
                    "IO error when reading to {}: {}. (in '{}')",
                    reference, err, current_proc
                );
            });
            self.update(reference, |n| {
                // Ensure reversibility.
                if !n.is_zero() {
                    error!(
                        "Reading to non-zero variable, '{}'. (in '{}')",
                        reference, current_proc
                    );
                }

                // Update variable to the read number.
                *n = str[..str.len() - 1].parse().unwrap_or_else(|err| {
                    error!(
                        "Failed to parse integer for '{}': {}. (in '{}')",
                        reference, err, current_proc
                    );
                });
            });
        } else {
            // Print and zero.
            self.update(reference, |n| {
                println!("{}", n);
                *n = 0.into();
            });
        }
    }

    /// Modify `reference` through closure `f`.
    fn update<F: Fn(&mut Number)>(&mut self, reference: Reference, f: F) {
        if let Binding::Val(ref mut n) = self.stack[reference.frame]
            .vars
            .get_mut(reference.name)
            .unwrap()
        {
            f(n);
        } else {
            panic!("Reference to reference.");
        }
    }

    /// Add (when `reverse` is `true`) or subtract (otherwise) `val` to `reference`.
    fn add_to(&mut self, reference: Reference, val: Number, reverse: bool) {
        // Get a mutable reference to the value.
        if let Binding::Val(n) = self.stack[reference.frame]
            .vars
            .get_mut(reference.name)
            .unwrap()
        {
            // We use the `take_mut` library for Maximal Efficiency™ to avoid reallocating the
            // `BigInt` and instead adding or subtracting in-place.
            take_mut::take(n, |n| if reverse { n - val } else { n + val })
        } else {
            panic!("Reference to reference.");
        }
    }

    /// Evaluate expression `expr`.
    ///
    /// This errors if the reference `disallow` (if any) is loaded in the expression.
    fn eval(&mut self, expr: &'a Expr, disallow: Option<Reference>) -> Number {
        // It appears that the compiler successfully optimizes this recursive call away with
        // tail-call optimization, thus the overhead is not any greater than using a folding loop.

        match expr {
            // Constants.
            &Expr::Number(ref n) => n.clone(),
            // Variable loading.
            &Expr::Load(ref var) => {
                let reference = self.get_ref(var);
                // Ensure that it is not disallowed.
                if let Some(illegal_ref) = disallow {
                    if illegal_ref == reference {
                        error!(
                            "Illegally loading '{}' while mutating it. (in '{}')",
                            reference,
                            self.current_proc()
                        );
                    }
                }
                self.load(reference)
            }
            // Unary operators.
            &Expr::Neg(ref e) => -self.eval(e, disallow),
            &Expr::Not(ref e) => into_num(!into_bool(self.eval(e, disallow))),
            // Comparison operators.
            &Expr::Le(ref e1, ref e2) => {
                into_num(self.eval(&e1, disallow) < self.eval(&e2, disallow))
            }
            &Expr::Leq(ref e1, ref e2) => {
                into_num(self.eval(&e1, disallow) <= self.eval(&e2, disallow))
            }
            &Expr::Gr(ref e1, ref e2) => {
                into_num(self.eval(&e1, disallow) > self.eval(&e2, disallow))
            }
            &Expr::Geq(ref e1, ref e2) => {
                into_num(self.eval(&e1, disallow) >= self.eval(&e2, disallow))
            }
            &Expr::Eq(ref e1, ref e2) => {
                into_num(self.eval(&e1, disallow) == self.eval(&e2, disallow))
            }
            &Expr::Neq(ref e1, ref e2) => {
                into_num(self.eval(&e1, disallow) != self.eval(&e2, disallow))
            }
            // Arithmetic operators.
            &Expr::Add(ref e1, ref e2) => self.eval(&e1, disallow) + self.eval(&e2, disallow),
            &Expr::Sub(ref e1, ref e2) => self.eval(&e1, disallow) - self.eval(&e2, disallow),
            &Expr::Mul(ref e1, ref e2) => self.eval(&e1, disallow) * self.eval(&e2, disallow),
            &Expr::Div(ref e1, ref e2) => {
                let denom = self.eval(&e2, disallow);
                if denom.is_zero() {
                    error!("Division by zero. (in '{}')", self.current_proc());
                }
                self.eval(&e1, disallow) / denom
            }
            &Expr::Rem(ref e1, ref e2) => {
                let denom = self.eval(&e2, disallow);
                if denom.is_zero() {
                    error!("Division by zero. (in '{}')", self.current_proc());
                }
                self.eval(&e1, disallow) % denom
            }
            // Logic operators.
            &Expr::Or(ref e1, ref e2) => {
                into_num(into_bool(self.eval(&e1, disallow)) || into_bool(self.eval(&e2, disallow)))
            }
            &Expr::And(ref e1, ref e2) => {
                into_num(into_bool(self.eval(&e1, disallow)) && into_bool(self.eval(&e2, disallow)))
            }
            &Expr::Xor(ref e1, ref e2) => {
                into_num(into_bool(self.eval(&e1, disallow)) ^ into_bool(self.eval(&e2, disallow)))
            }
        }
    }

    /// Execute `pr` (in reverse if `reverse` is `true`).
    fn exec(&mut self, pr: &'a Proc, reverse: bool) {
        if !reverse {
            // Execute every statement in order.
            for stat in pr.iter() {
                self.exec_stat(stat, reverse);
            }
        } else {
            // Execute every inverse statement in reverse order.
            for stat in pr.iter().rev() {
                self.exec_stat(stat, reverse);
            }
        }
    }

    /// Execute statement `stat` (in reverse if `reverse` is `true`).
    fn exec_stat(&mut self, stat: &'a Stat, reverse: bool) {
        match *stat {
            // x += y;
            Stat::Add(ref name, ref expr) => {
                let reference = self.get_ref(name);
                let res = self.eval(expr, Some(reference));
                self.add_to(reference, res, reverse);
            }
            // x -= y;
            Stat::Sub(ref name, ref expr) => {
                let reference = self.get_ref(name);
                let res = self.eval(expr, Some(reference));
                self.add_to(reference, res, !reverse);
            }
            // call func(...);
            Stat::Call(ref func) => self.call(func, reverse),
            // uncall func(...);
            Stat::Uncall(ref func) => self.call(func, !reverse),
            // if cond1 { ... } else { ... } fi cond2;
            Stat::If {
                ref in_condition,
                ref out_condition,
                ref body,
                ref body_else,
            } => {
                // Evaluate condition.
                let branch =
                    into_bool(self.eval(if reverse { out_condition } else { in_condition }, None));
                // Run `if` statement.
                if branch {
                    self.exec(body, reverse);
                } else {
                    self.exec(body_else, reverse);
                }

                // Ensure reversibility by checking that the conditions matches the taken branch.
                if into_bool(self.eval(if reverse { in_condition } else { out_condition }, None))
                    != branch
                {
                    error!("The exiting condition of 'if' statement does not match taken branch. (in '{}')", self.current_proc());
                }
            }
            // until cond { ... };
            Stat::Until { ref cond, ref body } => {
                // Ensure that the first condition is true (as specified).
                if !into_bool(self.eval(cond, None)) {
                    error!(
                        "Loop condition must be true at start. (in '{}')",
                        self.current_proc()
                    );
                }

                // Execute the body until the condition is true again.
                self.exec(body, reverse);
                while !into_bool(self.eval(cond, None)) {
                    self.exec(body, reverse);
                }
            }
            // print x;
            Stat::Print(ref name) => {
                let reference = self.get_ref(name);
                self.print(reference, reverse);
            }
            // read x;
            Stat::Read(ref name) => {
                let reference = self.get_ref(name);
                self.print(reference, !reverse);
            }
        }
    }

    /// Call procedure as specified by `call` (in reverse if `reverse` is `true`).
    fn call(&mut self, call: &'a Func, reverse: bool) {
        // Builtin procedures.
        match &*call.name {
            // Assert the values of an arbitrary number of variables are zero. If not, throw error.
            "assert_is_zero" => {
                for i in call.params.iter() {
                    let reference = self.get_ref(i); // Necessary to please the borrowchecker.
                    let val = self.load(reference);
                    if !val.is_zero() {
                        error!(
                            "'{}' is non-zero ({}) when calling 'assert_is_zero'. (in '{}')",
                            i,
                            val,
                            self.current_proc()
                        );
                    }
                }
                return;
            }
            // Assert the absolute values of an arbitrary number of variables are smaller than 100.
            // If not, throw error.
            "assert_is_small" => {
                for i in call.params.iter() {
                    let reference = self.get_ref(i); // Necessary to please the borrowchecker.
                    let val = self.load(reference);
                    if val.abs() >= 100.into() {
                        error!(
                            "'{}' is non-zero ({}) when calling 'assert_is_zero'. (in '{}')",
                            i,
                            val,
                            self.current_proc()
                        );
                    }
                }
                return;
            }
            // Write a number of variables to stdout with special marking (not a part of the main
            // "program output" that can be reversed).
            "debug" => {
                for i in call.params.iter() {
                    let reference = self.get_ref(i); // Necessary to please the borrowchecker.
                    eprintln!("DEBUG: {} = {}", i, self.load(reference));
                }
                return;
            }
            // Write to stderr to mark that a certain checkpoint has been reached.
            "checkpoint" => {
                for i in call.params.iter() {
                    eprintln!("DEBUG: checkpoint '{}'", i);
                }
                return;
            }
            // Crash the program.
            "trap" => error!("DEBUG: trap."),
            _ => {}
        }

        // Find the procedure to call.
        let (func, pr) = *self.procs.get(&*call.name).unwrap_or_else(|| {
            error!(
                "Called non-existent function, '{}'. (in '{}')",
                call.name,
                self.current_proc()
            )
        });

        // Check parameter number.
        if call.params.len() != func.params.len() {
            error!(
                "'{}' only accepts {} parameters; {} given. (in '{}')",
                func.name,
                func.params.len(),
                call.params.len(),
                self.current_proc(),
            );
        }

        // Create new frame with the parameters assigned.
        let mut vars = HashMap::new();
        for (param_name, passed_param) in func.params.iter().zip(call.params.iter()) {
            let reference = self.get_ref(&passed_param);
            if vars.insert(&**param_name, Binding::Ref(reference))
                .is_some()
            {
                error!("Declaring existing procedure '{}'.", call.name);
            };
        }
        self.stack.push(Frame {
            name: &func.name,
            vars,
        });

        // Execute the procedure.
        self.exec(pr, reverse);

        // Pop the frame from the stack (handles zeroing errors).
        self.pop_stack();
    }
}
