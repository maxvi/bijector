      ____  _ _           _
     |  _ \(_|_)         | |
     | |_) |_ _  ___  ___| |_ ___  _ __
     |  _ <| | |/ _ \/ __| __/ _ \| '__|
     | |_) | | |  __/ (__| || (_) | |
     |____/|_| |\___|\___|\__\___/|_|
            _/ |
           |__/      by Max Vistrup


Bijector is a programming language that guarantees by design that your program
defines an injective function (sparing crashes). Every state change is
reversible and the only way to get rid of information is by printing it using
`print`, which is inverse to `read` that reads from stdin. It is Turing
complete if you are allowed to truncate the output (and thus abandon the
reversibility).

This was my final submission for the 2018 edition of the "Programming Language
Design" course at DIKU, taught by Torben Ægidius Mogensen, who stated this very
interesting problem.

== Example: Prime factorization ===============================================
 1    # Swap data of a and b.
 2    procedure swap(a, b) {
 3        x += a;
 4        y += b;
 5
 6        a += y - x;
 7        b += x - y;
 8
 9        x -= b;
 10       y -= a;
 11   }
 12
 13   # Divide a by b, store the quotient in a and let b remain intact.
 14   procedure divide(a, b) {
 15       until a < b | q = 0 {
 16           a -= b;
 17           q += 1;
 18       };
 19       call swap(q, a);
 20   }
 21
 22   # Find lowest factor of n, store it in p, and divide it out of n.
 23   procedure factor1(n, p) {
 24       p += 1;
 25       # Trial division.
 26       until n % p = 0 & p != 1 | p = 1 {
 27           p += 1;
 28       };
 29
 30       # Divide out the factor.
 31       call divide(n, p);
 32   }
 33
 34   # Factorize n, printing the factors.
 35   procedure factor(n) {
 36       p += 1;
 37       # Extract factors until nothing left.
 38       until (n = 1) ^ (p = 1) {
 39           print p;
 40           call factor1(n, p);
 41       };
 42
 43       # Nullify.
 44       n -= 1;
 45       print p;
 46   }
 47
 48   procedure main() {
 49       read n;
 50       call factor(n);
 51   }
===============================================================================

How it works:
The semantics and syntax of the language is much like any other imperative
programming language. What makes programming languages irreversible is first
and foremost branching. To overcome this, following modifications are made.

- If-statements use the syntax `if P { code } fi Q`. If `a` is the state of the
  program and `f(a)` is the state after `f`, we ensure at runtime that `P(a) <=>
  Q(f(a))`. This implies that `Q(a) <=> P(f^{-1}(a))` and thus such conditional
  is reversible.
- Loops are created using `until P { code }`. The loop runs only if `P` is not
  satisfied, and ends once `P` is satisfied.

The language, being just a proof of concept, is fairly primitive. It only
allows signed integers. Variables are automatically initialized with value `0`,
and once they go out of scope, they are checked at runtime to be 0. As
mentioned in the beginning, the only way to get rid of entropy is by using
`print variable` (inverse: `read variable`), which not only prints `variable`
but also sets it to 0, thus effectively transfering the entropy from the state
of the program to the output.

Functions can be called using `call f(v1, ...)` and called in reverse using
`uncall f(v1, ...)`. There are no return values. Instead, the arguments are
passed as pointers and can be mutated inside the function.

Compiling bijector:
- Installing stable rustc and cargo.
    * For most Linux package managers, simply install the package `cargo`.
    * Otherwise, refer to https://www.rust-lang.org/en-US/install.html.
- Run `cargo build --release`.
- The binary will appear under `target/release/bijector`.

Running Bijector programs:
- We assume that you have compiled Bijecetor and that the binary is put in a
  directory in `$PATH` or similar. That is, the command is simply `bijector`.
- Run the command with the file name (e.g. `bijector program.bij`).
- To run a program in reverse, pass the `-r` flag, e.g.: `bijector -r
  program.bij`

File overview:

    ├── README.txt          : This file.
    ├── build.rs            : Pre-build code.
    ├── programs
    │  ├── factorial.bij    : The factorial program.
    │  ├── gcd.bij          : The GCD program.
    │  ├── hanoi.bij        : Program to illustrate non-trivial feature.
    │  └── prime.bij        : The factorization program.
    ├── src
    │  ├── grammar.rustpeg  : The rustpeg grammar.
    │  ├── main.rs          : The main files and code for CLI.
    │  ├── parse.rs         : Handles AST and parsing.
    │  └── state.rs         : Handles execution and program states.
    └── tests
       ├── fail_[...].bij   : Programs that should error.
       └── succ_[...].bij   : Programs that should succeed.
